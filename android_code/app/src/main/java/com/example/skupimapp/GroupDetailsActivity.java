package com.example.skupimapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.skupimapp.adapters.UserListAdapter;
import com.example.skupimapp.models.Group;
import com.example.skupimapp.models.User;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class GroupDetailsActivity extends AppCompatActivity {

    private RecyclerView userListRecyclerView;
    private ImageView groupPic;
    private TextView groupName, numMembers, totalOffers;
    private int offersNumber;
    private UserListAdapter adapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    String currentUserId = FirebaseAuth.getInstance().getUid();
    private Group currentGroup;
    public static final String usersInGroupsGroupInfo = "GROUP_INFO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("ON_CLICK","inside onCreate method");
        setContentView(R.layout.group_details);
        Intent intent = getIntent();
        if(intent!=null){
            currentGroup = (Group) intent.getSerializableExtra(UserGroupsActivity.currentGroup);

            if(intent.getStringArrayListExtra(UsersInGroupsActivity.userSender)!=null)
                updateAll(intent.getStringArrayListExtra(UsersInGroupsActivity.userSender));
        }
        userListRecyclerView = findViewById(R.id.group_details_RV);
        groupName = findViewById(R.id.group_details_name);
        groupPic = findViewById(R.id.group_details_pic);
        numMembers = findViewById(R.id.group_details_members);
        totalOffers = findViewById(R.id.group_details_total_offers);

        setRecyclerView();
        setOtherViews();
    }

    public void setRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        userListRecyclerView.setLayoutManager(layoutManager);

        Log.d("YOU SHALL NOT PASS","here");
        List<String> userList = currentGroup.getUserList();
        numMembers.setText("Members: "+(userList.size()+1));
        Query query;
        if (userList.size()==0) {
            userList.add("");
            query = db.collection("User").whereIn("id", userList);
            userList.remove("");
        }
        else
            query = db.collection("User").whereIn("id",userList);

        FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();
        adapter = new UserListAdapter(options);
        userListRecyclerView.setAdapter(adapter);
    }

    public void setOtherViews(){
        groupName.setText(currentGroup.getName());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.group_details_option_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.option_menu_leaveGroup:
                exitGroup();
                return true;
            case R.id.option_menu_addPeople:
                addPeople();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateAll(ArrayList<String> newUsersId){
        //update the current group
        for (String id : newUsersId){
            currentGroup.getUserList().add(id);
        }

        //run trensactions and replace the old group
        db.runTransaction(new Transaction.Function<Void>() {
            @Nullable
            @Override
            public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
//                DocumentSnapshot docUser =
                transaction.set(db.collection("User").document(currentUserId).collection("Group").document(currentGroup.getId()),currentGroup);
                transaction.set(db.collection("Group").document(currentGroup.getId()),currentGroup);

                for (String id : currentGroup.getUserList()){
                    transaction.set(db.collection("User").document(id).collection("Group").document(currentGroup.getId()),currentGroup);
                }
                return null;
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //updateRV
                numMembers.setText("Members: "+(currentGroup.getUserList().size()+1));
                Query query = db.collection("User").whereIn("id", currentGroup.getUserList());
                FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>()
                        .setQuery(query, User.class)
                        .build();
                adapter.updateOptions(options);
            }
        });

    }

    public void addPeople(){

        //must remember which group called UsersInGroupActivity
        Intent intent = new Intent(this,UsersInGroupsActivity.class);
        intent.putExtra(usersInGroupsGroupInfo,currentGroup);
        startActivity(intent);
        finish();
    }

    public void exitGroup(){
        if(currentGroup.getOwner().equals(currentUserId))
            removeOwner();
        else
            removeUser();

        CollectionReference collPost = db.collection("User").document(currentUserId).collection("Post");

        collPost.whereEqualTo("groupId",currentGroup.getId()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(final QuerySnapshot queryDocumentSnapshots) {

                final ArrayList<String> docPost = new ArrayList<>();

                //check does user have posts in current group
                if(queryDocumentSnapshots.getDocuments().size()!=0){
                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        docPost.add(documentSnapshot.getReference().getId());
                    }
                }


                db.runTransaction(new Transaction.Function<Void>() {
                    @Nullable
                    @Override
                    public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {

                        Log.d("GDA_CG","groupId : "+currentGroup.getId()+"\n groupOwner : "+currentGroup.getOwner()+"\n groupName : "+currentGroup.getName());

                        DocumentSnapshot docUserGroup = transaction.get(db.collection("User").document(currentUserId).collection("Group").document(currentGroup.getId()));
                        DocumentSnapshot docUser = transaction.get(db.collection("User").document(currentUserId));
                        DocumentSnapshot docGroup = transaction.get(db.collection("Group").document(currentGroup.getId()));

                        transaction.delete(docUserGroup.getReference());

                        for (String docReference : docPost) {
                            transaction.delete(docUser.getReference().collection("Post").document(docReference));
                            transaction.delete(docGroup.getReference().collection("Post").document(docReference));
                        }

                        //check if anyone is left in the group
                        if (currentGroup.getOwner().equals("")) {
                            transaction.delete(docGroup.getReference());
                        } else {
                            transaction.set(docGroup.getReference(), currentGroup);


                            transaction.set(db.collection("User").document(currentGroup.getOwner()).collection("Group").document(currentGroup.getId()),currentGroup);

                            for(String otherId : currentGroup.getUserList()){
                                if(!otherId.equals(""))
                                    transaction.set(db.collection("User").document(otherId).collection("Group").document(currentGroup.getId()),currentGroup);
                            }
                        }

                        return null;
                    }
                });


                }
        });

        startActivity(new Intent(this,MainPageActivity.class));

        }

    public void removeOwner(){

        //setting owner to blank, it will change only if someone can replace him
        currentGroup.setOwner("");
        if (currentGroup.getUserList().size()!=0){
            Random random = new Random();
            int index =  random.nextInt(currentGroup.getUserList().size());
            currentGroup.setOwner(currentGroup.getUserList().get(index));
            currentGroup.getUserList().remove(index);
        }
    }


    public void removeUser(){
        currentGroup.getUserList().remove(currentUserId);
    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }
    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
