package com.example.skupimapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.skupimapp.CreateGroup;
import com.example.skupimapp.CreateOfferActivity;
import com.example.skupimapp.MainPageActivity;
import com.example.skupimapp.R;
import com.example.skupimapp.models.User;


public class AddFragment extends Fragment {

    private ImageView groupAddPic,postAddPic;
    private User currentUser;
    public static final String addFragmentCurrentUser = "CURRENT_USER";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        View rootView = inflater.inflate(R.layout.add_fragment_layout,container,false);

        Bundle bundle = this.getArguments();
        if (bundle != null){

            currentUser =(User) bundle.getSerializable(MainPageActivity.userSender);
            Log.d("POST_USER",currentUser.getName());
        }

        groupAddPic = rootView.findViewById(R.id.add_fragment_addGroup);
        postAddPic = rootView.findViewById(R.id.add_fragment_addPost);

        setOnClickListeners();
        return rootView;
    }





    public void setOnClickListeners(){
        groupAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CreateGroup.class));
            }
        });

        postAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateOfferActivity.class);
                intent.putExtra(addFragmentCurrentUser,currentUser);
                startActivity(intent);
            }
        });

    }
}
