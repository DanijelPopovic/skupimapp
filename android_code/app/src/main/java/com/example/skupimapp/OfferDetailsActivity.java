package com.example.skupimapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.skupimapp.adapters.OfferView;
import com.example.skupimapp.adapters.UserListAdapter;
import com.example.skupimapp.fragments.AboutMeFragment;
import com.example.skupimapp.models.Group;
import com.example.skupimapp.models.User;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OfferDetailsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private UserListAdapter adapter;
    private OfferView offerView ;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_me);
        Intent intent = getIntent();
        if(intent!=null){
            offerView = (OfferView) intent.getSerializableExtra(AboutMeFragment.currentOffer);
        }
        setUpRecyclerView();
    }


    public void setUpRecyclerView(){
        recyclerView = findViewById(R.id.aboutMeFeed);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        List<String> userList = offerView.getLikeUserList();

        if(userList.contains(currentUserId))
            userList.remove(currentUserId);
        Query query = db.collection("User").whereIn("id",userList);

        FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();
        adapter = new UserListAdapter(options);
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }
    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
