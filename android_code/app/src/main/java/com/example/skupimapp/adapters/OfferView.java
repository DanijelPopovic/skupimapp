package com.example.skupimapp.adapters;

import java.io.Serializable;
import java.util.List;

public class OfferView implements Serializable {

    private String text;
    private String name;
    private String likeNumber;
    private String groupName;
    //contains list of user id that will get their xp increased
    private List<String> likeUserList;
    //contains owners id
    private String ownerId, groupId;

    public OfferView(String text, String name, String likeNumber,String groupName, List<String> likeUserList, String ownerId, String groupId) {
        this.text = text;
        this.name = name;
        this.likeNumber = likeNumber;
        this.groupName = groupName;
        this.likeUserList = likeUserList;
        this.ownerId = ownerId;
        this.groupId = groupId;
    }

    public OfferView() {
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public List<String> getLikeUserList() {
        return likeUserList;
    }

    public void setLikeUserList(List<String> likeUserList) {
        this.likeUserList = likeUserList;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(String likeNumber) {
        this.likeNumber = likeNumber;
    }
}
