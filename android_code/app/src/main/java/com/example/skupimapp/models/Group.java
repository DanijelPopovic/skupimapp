package com.example.skupimapp.models;

import android.net.Uri;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Group implements Serializable {

    private String name,imageUrl, owner, id;
    private List<String> userList;

    public Group(String name, List<String> userList, String imageUrl, String owner, String id) {
        this.name = name;
        this.userList = userList;
        this.imageUrl = imageUrl;
        this.owner = owner;
        this.id = id;
    }

    public Group() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUserList() {
        return userList;
    }

    public void setUserList(List<String> userList) {
        this.userList = userList;
    }
}
