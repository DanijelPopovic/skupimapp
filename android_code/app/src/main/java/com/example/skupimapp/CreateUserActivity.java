package com.example.skupimapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.skupimapp.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class CreateUserActivity extends AppCompatActivity {


    private EditText name;
    private Button imageChooser;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri imageUrl;
    private ImageView profileImage;
    private Button save;
    private String INTENT_PHONE_NUMBER = "phoneNumber";
    private String phoneNumber;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);


        Intent intent = getIntent();
        phoneNumber = intent.getStringExtra(INTENT_PHONE_NUMBER);

        save = findViewById(R.id.saveUser);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUser();
                goToCreateGroup();
            }
        });
        name = findViewById(R.id.userName);
        profileImage = findViewById(R.id.userProfileImage);
        imageChooser = findViewById(R.id.imageChooser);
        imageChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
    }

    public void goToCreateGroup(){
        startActivity(new Intent(this, CreateGroup.class));
    }

    public void openFileChooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int reslutCode, Intent data){
        super.onActivityResult(requestCode,reslutCode,data);

        if(requestCode==PICK_IMAGE_REQUEST && reslutCode==RESULT_OK
                && data!=null && data.getData()!=null){
            imageUrl = data.getData();
            profileImage.setImageURI(imageUrl);
        }

    }

    public void saveUser(){

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        final String userId = firebaseUser.getUid();
        final User user = new User(name.getText().toString(),phoneNumber,userId);
        user.setImageUrl(imageUrl.toString());



        db.collection("User").document(userId).set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(CreateUserActivity.this, "User saved with ID "+userId,Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(CreateUserActivity.this, "ERROR",Toast.LENGTH_SHORT).show();
                    }
                });
    }


}
