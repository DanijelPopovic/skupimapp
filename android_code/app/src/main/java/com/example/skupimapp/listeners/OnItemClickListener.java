package com.example.skupimapp.listeners;

import android.widget.ImageView;

import com.example.skupimapp.adapters.UserGroupsAdapter;
import com.example.skupimapp.models.Group;

public interface OnItemClickListener {
    void onItemClick(Group group);
}
