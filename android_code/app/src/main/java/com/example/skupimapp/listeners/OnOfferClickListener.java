package com.example.skupimapp.listeners;

import com.example.skupimapp.adapters.OfferView;


public interface OnOfferClickListener {
    void onItemClick(OfferView offer, String offerId);
}
