package com.example.skupimapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.skupimapp.MainPageActivity;
import com.example.skupimapp.R;
import com.example.skupimapp.adapters.OfferView;
import com.example.skupimapp.models.Offer;
import com.example.skupimapp.models.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    private SwitchCompat switchCompat;
    private EditText userName,userPhoneNumeber;
    private FloatingActionButton fab;
    private User currentUser;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    String currentUserId = FirebaseAuth.getInstance().getUid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);
        Intent intent = getIntent();

        if (intent!=null){
            currentUser = (User) intent.getSerializableExtra(MainPageActivity.userSender);
        }

        userName = findViewById(R.id.settings_layout_userName);
        userPhoneNumeber = findViewById(R.id.settings_layout_phoneNumber);
        fab = findViewById(R.id.settings_layout_save);
        switchCompat = findViewById(R.id.settings_layout_switchCompat);

        setUserInfo();
        disableChanges();
        addListeners();
    }


    public void setUserInfo(){
        userName.setText(currentUser.getName());
        userPhoneNumeber.setText(currentUser.getPhoneNumber());
    }

    public void disableChanges(){
        userName.setEnabled(false);
        userPhoneNumeber.setEnabled(false);
    }

    public void enableChanges(){
        userName.setEnabled(true);
        userPhoneNumeber.setEnabled(true);
    }

    public void addListeners(){
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    Log.d("SETTINGS","can't use");
                    disableChanges();
                }
                else{
                    Log.d("SETTINGS","can use");
                    enableChanges();
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userName.getText().equals(currentUser.getName()) && userPhoneNumeber.getText().equals(currentUser.getPhoneNumber()))
                    return;
                else {
                    currentUser.setName(userName.getText().toString());
                    currentUser.setPhoneNumber(userPhoneNumeber.getText().toString());
                    db.collection("User").document(currentUserId).set(currentUser);

                    db.collection("User").document(currentUserId).collection("Post").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            for(final DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                final OfferView offerView = documentSnapshot.toObject(OfferView.class);
                                db.runTransaction(new Transaction.Function<Void>() {
                                    @Nullable
                                    @Override
                                    public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                                        transaction.update(db.collection("User").document(currentUserId).collection("Post").
                                                    document(documentSnapshot.getId()),"name",currentUser.getName());
                                        transaction.update(db.collection("Group").document(offerView.getGroupId()).collection("Post")
                                                    .document(documentSnapshot.getId()),"name",currentUser.getName());
                                        return null;
                                    }
                                });
                            }
                        }
                    });

                }
            }
        });
    }
}
