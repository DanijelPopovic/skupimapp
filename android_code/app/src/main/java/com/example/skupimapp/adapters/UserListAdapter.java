package com.example.skupimapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skupimapp.R;
import com.example.skupimapp.listeners.OnUserAddListener;
import com.example.skupimapp.models.User;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class UserListAdapter extends FirestoreRecyclerAdapter<User,UserListAdapter.UserView> {

    private OnUserAddListener listener;
    private FirestoreRecyclerOptions<User> options;

    public UserListAdapter(@NonNull FirestoreRecyclerOptions<User> options) {
        super(options);
        this.options = options;
    }

    public UserListAdapter(@NonNull FirestoreRecyclerOptions<User> options, OnUserAddListener listener) {
        super(options);
        this.options = options;
        this.listener = listener;
    }

    @Override
    public void updateOptions(@NonNull FirestoreRecyclerOptions<User> options) {
        super.updateOptions(options);
        this.options = options;
    }

    @Override
    protected void onBindViewHolder(@NonNull UserView holder, final int position, @NonNull User model) {
        holder.userName.setText(model.getName());
        if(listener==null){
            holder.userCheck.setVisibility(View.INVISIBLE);
        }
        else{
            holder.userCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(options.getSnapshots().get(position));
                }
            });
        }

        if (holder.userCheck.isChecked()){
            holder.userCheck.setChecked(false);
        }
    }

    @NonNull
    @Override
    public UserView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_friends_view,parent,false);
        UserView holder = new UserView(view);
        return holder;
    }

    public static class UserView extends RecyclerView.ViewHolder{
        private ImageView userPic;
        private CheckBox userCheck;
        private TextView userName;

        public UserView(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.add_friends_view_name);
            userCheck = itemView.findViewById(R.id.add_friends_view_check);
            //add userPic later
        }
    }
}
