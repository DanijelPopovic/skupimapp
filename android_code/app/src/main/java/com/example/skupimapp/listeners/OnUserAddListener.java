package com.example.skupimapp.listeners;

import com.example.skupimapp.models.User;

public interface OnUserAddListener {
    void onItemClick(User user);
}
