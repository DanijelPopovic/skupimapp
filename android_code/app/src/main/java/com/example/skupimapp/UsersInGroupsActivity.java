package com.example.skupimapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.skupimapp.adapters.UserListAdapter;
import com.example.skupimapp.listeners.OnUserAddListener;
import com.example.skupimapp.models.User;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsersInGroupsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private UserListAdapter adapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    String currentUserId = FirebaseAuth.getInstance().getUid();
    private FirestoreRecyclerOptions<User> options;
    private ArrayList<String> addedUsers = new ArrayList<>(), userImages = new ArrayList<>();
    private EditText userToFind;
    private RadioGroup choice;
    FloatingActionButton addFab, searchFab;
    private OnUserAddListener listener = new OnUserAddListener() {
        @Override
        public void onItemClick(User user) {
            updateAddedUsers(user);
        }
    };
    public static final String userSender = "USER_SENDER";
    public static final String userImageSender = "USER_IMAGE_SENDER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_friends);

        recyclerView = findViewById(R.id.add_friends_RV);
        choice = findViewById(R.id.add_friends_choice);
        userToFind = findViewById(R.id.add_friends_userSearchParam);
        addFab = findViewById(R.id.add_friends_save);
        addFab.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        searchFab = findViewById(R.id.add_friends_search);
        searchFab.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
//        addedUsers.add("");

        setRecyclerView();
        setListeners();
    }


    public void setRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String placeHolder = "";

        Query query = db.collection("User").whereEqualTo("id",placeHolder);

        options = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();
        adapter = new UserListAdapter(options,listener);
        recyclerView.setAdapter(adapter);

//        if(options.getSnapshots().size()==0){
//            Toast.makeText(this,"No number found",Toast.LENGTH_SHORT).show();
//        }
    }

    public ArrayList<String> getContactList(){
        ArrayList<String> result = new ArrayList<>();
        Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
        while(cursor.moveToNext()){
            String phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            result.add(phone);
            if(result.size()==9)
                break;
        }
        return result;
    }

    public void updateAddedUsers(User user){
        if(addedUsers.contains(user.getId())){
            addedUsers.remove(user.getId());
            userImages.remove(user.getImageUrl());
        }
        else{
            addedUsers.add(user.getId());
            userImages.add(user.getImageUrl());
        }

    }

    public void setListeners(){
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAddedUsers();
            }
        });

        searchFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchFireStore();
            }
        });
    }

    public void sendAddedUsers(){
        Intent intent;
        if(getIntent().getSerializableExtra(GroupDetailsActivity.usersInGroupsGroupInfo)!=null) {
            intent = new Intent(this, GroupDetailsActivity.class);
            intent.putExtra(UserGroupsActivity.currentGroup, getIntent().getSerializableExtra(GroupDetailsActivity.usersInGroupsGroupInfo));
        }
        else{
            intent = new Intent(this,CreateGroup.class);
            intent.putStringArrayListExtra(userImageSender,userImages);
        }
        intent.putStringArrayListExtra(userSender,addedUsers);

        startActivity(intent);
        finish();
    }

    public void searchFireStore(){
        String searchParametar;
        switch (choice.getCheckedRadioButtonId()){
            case R.id.add_friends_choice_name:
                searchParametar = "name";
                break;
            case R.id.add_friends_choice_phone:
                searchParametar = "phoneNumber";
                break;
            default:
                searchParametar=null;
                break;
        }
        if(searchParametar==null){
            Toast.makeText(this,"Choose which type to search by",Toast.LENGTH_SHORT).show();
            return;
        }

        if(userToFind.getText().toString().matches("")){
            Toast.makeText(this,"Enter phonenumber or name",Toast.LENGTH_SHORT).show();
            return;
        }

        Query query = db.collection("User").whereEqualTo(searchParametar,userToFind.getText().toString());
        options = new FirestoreRecyclerOptions.Builder<User>()
                .setQuery(query, User.class)
                .build();

        adapter.updateOptions(options);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }
    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
