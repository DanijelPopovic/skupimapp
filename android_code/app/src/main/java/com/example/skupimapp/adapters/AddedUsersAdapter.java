package com.example.skupimapp.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skupimapp.R;
import com.example.skupimapp.models.User;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class AddedUsersAdapter extends FirestoreRecyclerAdapter<User,AddedUsersAdapter.AddedUsersView> {


    public AddedUsersAdapter(@NonNull FirestoreRecyclerOptions<User> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull AddedUsersView holder, int position, @NonNull User model) {
    }

    @NonNull
    @Override
    public AddedUsersView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_view,parent,false);
        AddedUsersView holder = new AddedUsersView(view);
        return holder;
    }

    public static class AddedUsersView extends RecyclerView.ViewHolder {

        private ImageView userPic;

        public AddedUsersView(@NonNull View itemView) {
            super(itemView);
            userPic = itemView.findViewById(R.id.default_group_img);
        }
    }
}
