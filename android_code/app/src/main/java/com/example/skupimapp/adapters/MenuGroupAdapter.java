package com.example.skupimapp.adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skupimapp.R;
import com.example.skupimapp.listeners.OnItemClickListener;
import com.example.skupimapp.models.Group;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class MenuGroupAdapter extends FirestoreRecyclerAdapter<Group, MenuGroupAdapter.GroupView> {

    private OnItemClickListener listener;
    private FirestoreRecyclerOptions<Group> options;

    public MenuGroupAdapter(@NonNull FirestoreRecyclerOptions<Group> options, OnItemClickListener listener) {
        super(options);
        this.listener = listener;
        this.options = options;
    }

    @Override
    protected void onBindViewHolder(@NonNull GroupView holder, final int position, @NonNull Group model) {
        //holder.groupImage.setImageURI(Uri.parse(model.getImageUrl()));
        holder.groupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(options.getSnapshots().get(position));
            }
        });
    }

    @NonNull
    @Override
    public GroupView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_view,parent,false);
        GroupView holder = new GroupView(view);
        return holder;
    }

    public static class GroupView extends RecyclerView.ViewHolder{

        private ImageView groupImage;

        public GroupView(@NonNull View itemView) {
            super(itemView);
            groupImage = itemView.findViewById(R.id.default_group_img);
        }
    }
}
