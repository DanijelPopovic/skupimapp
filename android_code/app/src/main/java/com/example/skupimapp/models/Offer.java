package com.example.skupimapp.models;

public class Offer {

    private String offer;
    private String creator;

    public Offer() {
    }

    public Offer(String offer, String creator) {
        this.offer = offer;
        this.creator = creator;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }
}
