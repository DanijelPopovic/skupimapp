package com.example.skupimapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.skupimapp.adapters.UserGroupsAdapter;
import com.example.skupimapp.listeners.OnItemClickListener;
import com.example.skupimapp.models.Group;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class UserGroupsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private UserGroupsAdapter adapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    String currentUserId = FirebaseAuth.getInstance().getUid();
    public static final String currentGroup = "CURRENT_GROUP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_me);
        recyclerView = findViewById(R.id.aboutMeFeed);
        setRecycleView();
    }


    public void setRecycleView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        Query query = db.collection("User").document(currentUserId).collection("Group").orderBy("name", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<Group> options = new FirestoreRecyclerOptions.Builder<Group>()
                .setQuery(query, Group.class)
                .build();
        adapter = new UserGroupsAdapter(options, new OnItemClickListener() {
            @Override
            public void onItemClick(Group group) {
                Intent intent = new Intent(getApplicationContext(),GroupDetailsActivity.class);
                intent.putExtra(currentGroup,group);
                Log.d("ON_CLICK","inside the method");
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);


    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }
    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
