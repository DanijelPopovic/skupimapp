package com.example.skupimapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.skupimapp.adapters.OfferView;
import com.example.skupimapp.fragments.AddFragment;
import com.example.skupimapp.models.Group;
import com.example.skupimapp.models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateOfferActivity extends AppCompatActivity {

    private Spinner spinner;
    private EditText userPost;
    FirebaseFirestore db =  FirebaseFirestore.getInstance();
    private String currentGroup,currentGroupID;
    private String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    ArrayAdapter<String> dataAdapter;
    private User currentUser;
    public static final String addFragment = "ADD_FRAGMENT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        Intent intent = getIntent();
        if (intent!=null){
            currentUser = (User) intent.getSerializableExtra(AddFragment.addFragmentCurrentUser);
        }

        final List<String> groupList = new ArrayList<>();

        groupList.add("Choose group");

        db.collection("User").document(currentUserId).collection("Group").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    Group group = documentSnapshot.toObject(Group.class);
                    String groupName = group.getName();
                    groupList.add(groupName);
                }
            }
        });

        userPost = findViewById(R.id.userNextPost);
        spinner = findViewById(R.id.simpleSpinner);
        dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,groupList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentGroup = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FloatingActionButton fab = findViewById(R.id.postAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePost();
            }
        });
    }



    public void savePost(){

        final String post = userPost.getText().toString();


        db.collection("Group").whereEqualTo("name",currentGroup).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    String groupId = documentSnapshot.getId();
                    DocumentReference docRef = db.collection("Group").document(groupId).collection("Post").document();
                    OfferView offerView = new OfferView(post,currentUser.getName(),"0",currentGroup, Arrays.asList(""),currentUserId, groupId);
                    db.collection("Group").document(groupId).collection("Post").document(docRef.getId()).set(offerView).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("bad",e.getMessage());
                        }
                    });
                    db.collection("User").document(currentUserId).collection("Post").document(docRef.getId()).set(offerView).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("userPostError",e.getMessage());
                        }
                    });

                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("prvi",e.getMessage());
            }
        });

        Intent intent = new Intent(this, MainPageActivity.class);
        intent.putExtra(addFragment,"offer_added");
        startActivity(intent);
    }
}
