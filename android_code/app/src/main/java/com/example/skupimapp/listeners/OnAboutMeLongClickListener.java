package com.example.skupimapp.listeners;

import com.example.skupimapp.adapters.OfferView;

public interface OnAboutMeLongClickListener {
    void onLongClick(int offerPosition, String offerDecision);
}
