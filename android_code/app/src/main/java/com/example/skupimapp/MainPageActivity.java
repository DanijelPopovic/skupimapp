package com.example.skupimapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.skupimapp.adapters.MenuGroupAdapter;
import com.example.skupimapp.fragments.AboutMeFragment;
import com.example.skupimapp.fragments.AddFragment;
import com.example.skupimapp.fragments.FeedFragment;
import com.example.skupimapp.models.User;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class MainPageActivity extends AppCompatActivity {

    private RecyclerView groupRecyclerView;
    private MenuGroupAdapter adapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    String currentUserId = FirebaseAuth.getInstance().getUid();
    private User currentUser;
    String currentSelectedGroup;
    public static final String dataSender = "DATA_SENDER";
    public static final String userSender = "USER_SENDER";
    public static final String logout = "LOGOUT";

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            Fragment selected = null;
            switch (menuItem.getItemId()) {
                case R.id.feed:
                    selected = new FeedFragment();
                    break;
                case R.id.aboutMe:
                    selected = new AboutMeFragment();
                    break;
                case R.id.add:
                    selected = new AddFragment();
                    break;
            }

            Bundle bundle = new Bundle();
            bundle.putString(dataSender,currentSelectedGroup);
            bundle.putSerializable(userSender,currentUser);
            selected.setArguments(bundle);
            Log.d("USER",currentUser.getName());
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selected).commit();
            return true;
        }
    };

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main_page);
            Intent intent = getIntent();

//            checking if user has logged out
            boolean finish = intent.getBooleanExtra(logout,false);
            if (finish){
                startActivity(new Intent(this,MainActivity.class));
                finish();
                return;
            }

            getCurrentSelectedGroup();
            getCurrentUser();
            BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
            bottomNavigationView.setOnNavigationItemSelectedListener(navListener);



            if (savedInstanceState == null) {
                Fragment selected;
                if(intent.getStringExtra(CreateOfferActivity.addFragment)==null)
                     selected = new FeedFragment();
                else{
                    selected = new AddFragment();
                    bottomNavigationView.setSelectedItemId(R.id.add);
                }

                Bundle bundle = new Bundle();
                bundle.putString(dataSender,currentSelectedGroup);
                bundle.putSerializable(userSender,currentUser);
                selected.setArguments(bundle);
                Log.d("USER",currentUser.getName());
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selected).commit();
            }


        }



    public void getCurrentSelectedGroup(){
        Task<QuerySnapshot> result = db.collection("User").document(currentUserId).collection("Group").limit(1).get();
        while(true){
            if(result.isComplete()){
                try {
                    currentSelectedGroup=result.getResult().getDocuments().get(0).getId();
                    break;
                }
                catch (IndexOutOfBoundsException ex){
                    currentSelectedGroup = "";
                    break;
                }

            }
        }
    }

    public void getCurrentUser(){
            Task<DocumentSnapshot> result = db.collection("User").document(currentUserId).get();
            while(true){
                if(result.isComplete()){
                    currentUser = result.getResult().toObject(User.class);
                    break;
                }
            }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.about_me_option_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.logout_menu_item:
                FirebaseAuth.getInstance().signOut();
                intent = new Intent(getApplicationContext(), MainPageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(logout, true);
                startActivity(intent);
                finish();
                return true;
            case R.id.groups_menu_item:
                intent = new Intent(getApplicationContext(), UserGroupsActivity.class);
                startActivity(intent);
                return true;
            case R.id.settings_menu_item:
                intent = new Intent(getApplicationContext(), SettingsActivity.class);
                intent.putExtra(userSender,currentUser);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
