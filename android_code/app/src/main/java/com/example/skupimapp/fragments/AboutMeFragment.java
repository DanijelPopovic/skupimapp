package com.example.skupimapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skupimapp.MainActivity;
import com.example.skupimapp.OfferDetailsActivity;
import com.example.skupimapp.R;
import com.example.skupimapp.adapters.OfferView;
import com.example.skupimapp.adapters.SimpleAdapter;
import com.example.skupimapp.listeners.OnAboutMeLongClickListener;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;

public class AboutMeFragment extends Fragment {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private SimpleAdapter adapter;
    private RecyclerView recyclerView;
    private FirestoreRecyclerOptions<OfferView> options;
    String currentUserId = FirebaseAuth.getInstance().getUid();
    public static final String currentOffer = "CURRENT_OFFER";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();

        View rootView = inflater.inflate(R.layout.about_me, container, false);

        setUpUserRecyclerView(rootView);

        return rootView;

    }


    public void setUpUserRecyclerView(View view){

        recyclerView = view.findViewById(R.id.aboutMeFeed);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        Query query = db.collection("User").document(currentUserId).collection("Post").orderBy("text", Query.Direction.DESCENDING);
        options = new FirestoreRecyclerOptions.Builder<OfferView>()
                .setQuery(query, OfferView.class)
                .build();

        adapter = new SimpleAdapter(options, new OnAboutMeLongClickListener() {
            @Override
            public void onLongClick(int offerPosition, String offerDecision) {
                Log.d("ABOUT_ME","in");
                switch (offerDecision){
                    case "Expire":
                        expireOffer(offerPosition);
                        break;
                    case "Delete":
                        deleteOffer(offerPosition);
                        break;
                    case "Details":
                        detailsOffer(offerPosition);
                        break;
                }
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onStart(){
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
    }

    @Override
    public void onStop(){
        super.onStop();
        if(adapter!=null){
            adapter.stopListening();
        }
    }

    public void expireOffer(int position){
        final String offerId = options.getSnapshots().getSnapshot(position).getId();
        final String offerGroupId = options.getSnapshots().get(position).getGroupId();


        db.runTransaction(new Transaction.Function<Void>() {
            @Nullable
            @Override
            public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                DocumentReference docRefUser = db.collection("User").document(currentUserId).collection("Post").document(offerId);
                DocumentReference docRefGroup = db.collection("Group").document(offerGroupId).collection("Post").document(offerId);
                transaction.delete(docRefUser);
                transaction.delete(docRefGroup);
                return null;
            }
        });

    }

    public void deleteOffer(int position){
        final String offerId = options.getSnapshots().getSnapshot(position).getId();
        final String offerGroupId = options.getSnapshots().get(position).getGroupId();


        db.runTransaction(new Transaction.Function<Void>() {
            @Nullable
            @Override
            public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                DocumentReference docRefUser = db.collection("User").document(currentUserId).collection("Post").document(offerId);
                DocumentReference docRefGroup = db.collection("Group").document(offerGroupId).collection("Post").document(offerId);
                transaction.delete(docRefUser);
                transaction.delete(docRefGroup);
                return null;
            }
        });
    }

    public void detailsOffer(int position){
        if(options.getSnapshots().get(position).getLikeUserList().size()==1){
            Toast.makeText(getActivity(),"No one has liked your post",Toast.LENGTH_SHORT).show();
        }
        else {
            Intent intent = new Intent(getActivity(), OfferDetailsActivity.class);
            intent.putExtra(currentOffer, options.getSnapshots().get(position));
            startActivity(intent);
        }
    }
}
