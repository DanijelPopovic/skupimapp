package com.example.skupimapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.skupimapp.adapters.AddedUsersAdapter;
import com.example.skupimapp.models.Group;
import com.example.skupimapp.models.User;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateGroup extends AppCompatActivity {


    private EditText groupName;
    private ImageView groupImage, addPeople;
    private Uri imageUrl;
    private static final int PICK_IMAGE_REQUEST = 1;
    private FloatingActionButton saver;
    private List<String> userList;
    private AddedUsersAdapter adapter;
    private String currentUserId = FirebaseAuth.getInstance().getUid();
    private RecyclerView recyclerView;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);


        groupImage = findViewById(R.id.groupImage);
        groupName = findViewById(R.id.groupName);
        addPeople = findViewById(R.id.create_group_addPeople);
        saver = findViewById(R.id.groupSaver);
        recyclerView = findViewById(R.id.create_group_RV);
        setRecyclerView();
        addListeners();

    }

    public void addListeners(){
        groupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
        addPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seeContactUsers();
            }
        });
        saver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveGroup();
            }
        });
    }

    public void goToMainPage(){
        Intent intent = new Intent(this, MainPageActivity.class);
        startActivity(intent);
    }

    public void seeContactUsers(){
        startActivity(new Intent(this,UsersInGroupsActivity.class));
    }

    public void openFileChooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int reslutCode, Intent data){
        super.onActivityResult(requestCode,reslutCode,data);

        if(requestCode==PICK_IMAGE_REQUEST && reslutCode==RESULT_OK
                && data!=null && data.getData()!=null){
            imageUrl = data.getData();
            groupImage.setImageURI(imageUrl);
        }

    }


    public void saveGroup(){


        final String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Intent intent = getIntent();
        if(intent.getStringArrayListExtra(UsersInGroupsActivity.userSender)!=null){
            userList = intent.getStringArrayListExtra(UsersInGroupsActivity.userSender);
//            userList.add(0,"");
        }
        else{
            userList = new ArrayList<>();
//            userList.add("");
        }


        db.runTransaction(new Transaction.Function<Void>() {
            @Nullable
            @Override
            public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                DocumentSnapshot doc =  transaction.get(db.collection("Group").document());
                Group group = new Group(groupName.getText().toString(),userList,imageUrl.toString(), currentUserId,doc.getReference().getId());

                transaction.set(db.collection("Group").document(doc.getId()),group);
                transaction.set(db.collection("User").document(currentUserId).collection("Group").document(doc.getId()), group);

                for (String otherId : userList){
                    transaction.set(db.collection("User").document(otherId).collection("Group").document(doc.getId()),group);
                }
                return null;
            }
        });
//        db.collection("Group").document(doc.getId()).set(group);
//        db.collection("User").document(currentUserId).collection("Group").document(doc.getId()).set(group);

        Intent back = new Intent(this,MainPageActivity.class);
        back.putExtra(CreateOfferActivity.addFragment,"yes");
        startActivity(back);
    }


    public void setRecyclerView(){

        Intent intent = getIntent();
        ArrayList<String> userList;
        if (intent.getStringArrayListExtra(UsersInGroupsActivity.userSender)!=null){
            if(intent.getStringArrayListExtra(UsersInGroupsActivity.userSender).size()!=0){
                Log.d("SENDING_SUCCESS","intent is not null");
                for (String x : intent.getStringArrayListExtra(UsersInGroupsActivity.userSender))
                    Log.d("SENDING_SUCCESS",x);
                userList = intent.getStringArrayListExtra(UsersInGroupsActivity.userSender);
                Query query = db.collection("User").whereIn("id",userList);
                FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>()
                        .setQuery(query, User.class)
                        .build();

                adapter = new AddedUsersAdapter(options);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));
            }
        }
    }


    @Override
    public void onStart(){
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
    }



    @Override
    public void onStop(){
        super.onStop();
        if(adapter!=null){
            adapter.stopListening();
        }
    }
}
