package com.example.skupimapp.models;

import java.io.Serializable;

public class User implements Serializable {

    private String name;
    private String phoneNumber;
    private String imageUrl;
    private String id;


    public User(String name, String phoneNumber,String id) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.id = id;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
