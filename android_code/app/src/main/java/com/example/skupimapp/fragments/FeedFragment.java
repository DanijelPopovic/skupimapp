package com.example.skupimapp.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skupimapp.MainPageActivity;
import com.example.skupimapp.R;
import com.example.skupimapp.adapters.MenuGroupAdapter;
import com.example.skupimapp.adapters.OfferView;
import com.example.skupimapp.adapters.SimpleAdapter;
import com.example.skupimapp.listeners.OnItemClickListener;
import com.example.skupimapp.listeners.OnOfferClickListener;
import com.example.skupimapp.models.Group;
import com.example.skupimapp.models.User;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Transaction;


public class FeedFragment extends Fragment{

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private SimpleAdapter adapter;
    private MenuGroupAdapter groupAdapter;
    private RecyclerView recyclerView, groupRecyclerView;
    String currentUserId = FirebaseAuth.getInstance().getUid();
    private String currentGroup;
    private User currentUser;


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        View rootView =  inflater.inflate(R.layout.simple_layout, container, false);
        Bundle bundle =this.getArguments();
        if (bundle!=null && bundle.getString(MainPageActivity.dataSender)!=null){
            currentGroup = bundle.getString(MainPageActivity.dataSender);
            currentUser = (User) bundle.getSerializable(MainPageActivity.userSender);
        }

        recyclerView = rootView.findViewById(R.id.simpleRV);
        groupRecyclerView = rootView.findViewById(R.id.menuRecyclerView);

        setGroupRecyclerView();
        setFeedRecyclerView();

        return rootView;
    }

    public void setGroupRecyclerView(){
        LinearLayoutManager grouplayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        groupRecyclerView.setLayoutManager(grouplayoutManager);

        Query groupQuery = db.collection("User").document(currentUserId).collection("Group").orderBy("name", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<Group> groupOptions = new FirestoreRecyclerOptions.Builder<Group>()
                .setQuery(groupQuery, Group.class)
                .build();
        groupAdapter = new MenuGroupAdapter(groupOptions, new OnItemClickListener() {
            @Override
            public void onItemClick(Group group) {
                currentGroup = group.getId();
                changeFeedFragmentRecyclerView(group);
            }
        });
        groupRecyclerView.setAdapter(groupAdapter);
    }

    public void setFeedRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        Query query = db.collection("Group").document(currentGroup).collection("Post").orderBy("text", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<OfferView> options = new FirestoreRecyclerOptions.Builder<OfferView>()
                .setQuery(query, OfferView.class)
                .build();

        adapter = new SimpleAdapter(options, new OnOfferClickListener() {
            @Override
            public void onItemClick(OfferView offerView, String offerId) {
                executeTransactions(offerView, offerId);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    public void changeFeedFragmentRecyclerView(Group group){

        Query query = db.collection("Group").document(group.getId()).collection("Post").orderBy("text", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<OfferView> options = new FirestoreRecyclerOptions.Builder<OfferView>()
                .setQuery(query, OfferView.class)
                .build();
        adapter.setOptions(options);
        adapter.updateOptions(options);
    }


    @Override
    public void onStart(){
        super.onStart();
        if(adapter!=null){
            adapter.startListening();
        }
        if(groupAdapter!=null){
            groupAdapter.startListening();
        }
    }



    @Override
    public void onStop(){
        super.onStop();
        if(adapter!=null){
            adapter.stopListening();
        }
        if(groupAdapter!=null){
            groupAdapter.stopListening();
        }
    }

    public void executeTransactions(final OfferView offerView, final String offerId){
        if(!offerView.getOwnerId().equals(currentUserId)) {
            db.runTransaction(new Transaction.Function<Void>() {
                @Nullable
                @Override
                public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                    DocumentReference docUser = db.collection("User").document(offerView.getOwnerId()).collection("Post").document(offerId);
                    DocumentReference docGroup = db.collection("Group").document(currentGroup).collection("Post").document(offerId);
                    transaction.set(docUser, offerView);
                    transaction.set(docGroup, offerView);
                    return null;
                }
            });
        }
    }


}
