package com.example.skupimapp.adapters;

import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skupimapp.R;
import com.example.skupimapp.listeners.OnAboutMeLongClickListener;
import com.example.skupimapp.listeners.OnOfferClickListener;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;

public class SimpleAdapter extends FirestoreRecyclerAdapter<OfferView, SimpleAdapter.SimpleViewHolder> {

    private OnOfferClickListener listener;
    private OnAboutMeLongClickListener longListener;
    private FirestoreRecyclerOptions<OfferView> options;
    private String currentUserId = FirebaseAuth.getInstance().getUid();


    public SimpleAdapter(FirestoreRecyclerOptions<OfferView> options){
        super(options);
        this.options = options;
        Log.d("CREATING1","inside_constructor");
    }

    public SimpleAdapter(FirestoreRecyclerOptions<OfferView> options, OnOfferClickListener listener){
        super(options);
        this.options = options;
        this.listener = listener;
        Log.d("CREATING1","inside_constructor");
    }

    public SimpleAdapter(FirestoreRecyclerOptions<OfferView> options, OnAboutMeLongClickListener longListener){
        super(options);
        this.options = options;
        this.longListener = longListener;
        Log.d("CREATING1","inside_constructor");
    }


    @Override
    protected void onBindViewHolder(@NonNull final SimpleViewHolder holder, final int position, @NonNull OfferView model) {
        Log.d("CREATING2",model.getText());
        holder.userPost.setText(model.getText());
        holder.userName.setText(model.getName());
        holder.likeNumber.setText(model.getLikeNumber());
        holder.groupName.setText("@"+model.getGroupName());


        if (listener!=null){
            holder.coffeeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OfferView offerView = options.getSnapshots().get(position);
                    if(offerView.getLikeUserList().contains(currentUserId)) {
                        offerView.getLikeUserList().remove(currentUserId);
                        int newValue = Integer.parseInt(offerView.getLikeNumber())-1;
                        offerView.setLikeNumber(String.valueOf(newValue));
                    }
                    else {
                        offerView.getLikeUserList().add(currentUserId);
                        int newValue = Integer.parseInt(offerView.getLikeNumber())+1;
                        offerView.setLikeNumber(String.valueOf(newValue));
                    }
                    listener.onItemClick(offerView, options.getSnapshots().getSnapshot(position).getId());

                }
            });
        }

        if (longListener!=null){
            holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add(position,position,position,"Expire").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            longListener.onLongClick(position,item.getTitle().toString());
                            return false;
                        }
                    });

                    menu.add(position,position,position,"Delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            longListener.onLongClick(position,item.getTitle().toString());
                            return false;
                        }
                    });

                    menu.add(position,position,position,"Details").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            longListener.onLongClick(position,item.getTitle().toString());
                            return false;
                        }
                    });

                }

            });
        }

    }

    public FirestoreRecyclerOptions<OfferView> getOptions() {
        return options;
    }

    public void setOptions(FirestoreRecyclerOptions<OfferView> options) {
        this.options = options;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {

        private ImageView userImage, coffeeButton;
        private TextView userName;
        private TextView userPost;
        private TextView likeNumber;
        private TextView groupName;

        public SimpleViewHolder(final View view) {
            super(view);
            this.userImage = view.findViewById(R.id.profileImage);
            this.userName = view.findViewById(R.id.name);
            this.userPost = view.findViewById(R.id.post);
            this.likeNumber = view.findViewById(R.id.likeNumber);
            this.groupName = view.findViewById(R.id.postGroupName);
            this.coffeeButton = view.findViewById(R.id.coffeeButton);

        }

    }


    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("CREATING3","inside_on_create");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_view,parent,false);
        SimpleViewHolder holder = new SimpleViewHolder(view);
        return holder;
    }


}
