package com.example.skupimapp.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.skupimapp.listeners.OnItemClickListener;
import com.example.skupimapp.R;
import com.example.skupimapp.models.Group;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

public class UserGroupsAdapter extends FirestoreRecyclerAdapter<Group,UserGroupsAdapter.GroupVeiw> {

    private OnItemClickListener listener;
    private FirestoreRecyclerOptions<Group> options;

    public UserGroupsAdapter(@NonNull FirestoreRecyclerOptions<Group> options, OnItemClickListener listener) {
        super(options);
        this.options = options;
        this.listener = listener;
        Log.d("IN","inside of adapter constructer");
        Log.d("IN",String.valueOf(options.getSnapshots().toArray().length));
    }

    @NonNull
    @Override
    public GroupVeiw onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("IN","inside of adapter onCreate method");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_list_view,parent,false);
        GroupVeiw holder = new GroupVeiw(view);
        return holder;
    }

    @Override
    protected void onBindViewHolder(@NonNull final GroupVeiw holder, final int position, @NonNull Group model) {
        Log.d("IN","inside of adapter onBindView method");
        holder.groupName.setText(model.getName());
        holder.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(options.getSnapshots().get(position));
            }
        });
    }

    public static class GroupVeiw extends RecyclerView.ViewHolder{
        private ImageView groupPic;
        private ImageView info;
        private TextView groupName;

        public GroupVeiw(@NonNull View itemView) {
            super(itemView);
            Log.d("IN","inside of group view constructer");
            groupName = itemView.findViewById(R.id.group_list_view_name);
            groupPic = itemView.findViewById(R.id.group_list_view_pic);
            info = itemView.findViewById(R.id.group_list_view_info);
        }
    }

    public OnItemClickListener getListener() {
        return listener;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
